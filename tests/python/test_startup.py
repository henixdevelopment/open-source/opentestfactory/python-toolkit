# Copyright (c) 2022-2023 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Expressions unit tests."""

import logging
import unittest

from unittest.mock import MagicMock, patch, mock_open
from subprocess import CalledProcessError

from opentf.scripts import startup


########################################################################

ENV_SSH_FULL = {
    'SSH_CHANNEL_HOST': 'hOSt',
    'SSH_CHANNEL_USER': 'usEr',
    'SSH_CHANNEL_TAGS': 'ta,gs',
    'SSH_CHANNEL_PASSWORD': 'passWorD',
}

SSHEE_YAML = '''apiVersion: opentestfactory.org/v1alpha1
kind: SSHServiceConfig
current-context: default
contexts:
- context:
    port: 443
    host: 127.0.0.1
    ssl_context: adhoc
    logfile: sshee.log
    eventbus:
      endpoint: https://127.0.0.1:38368
      token: reuse
    targets: [ssh-dummy]
  name: default
- context:
    port: 7786
    host: 127.0.0.1
    ssl_context: disabled
    trusted_authorities:
    - /etc/squashtf/*
    logfile: sshee.log
    enable_insecure_login: true
    eventbus:
        endpoint: http://127.0.0.1:38368
        token: reuse
    targets: [agents]
  name: allinone
'''

OPENTF_YAML = '''# squashtf.yaml
eventbus: python3 -m opentf.core.eventbus
services: []
plugins: []
disabled:
- dummyee
- HelloWorld
- localpublisher
'''

CA_BUNDLE = '''
-----BEGIN CERTIFICATE-----
MIIFgzCCBSqgAwIBAgIQAnwfbpkDeqyeUOVDa/9mpjAKBggqhkjOPQQDAjBKMQsw
CQYDVQQGEwJVUzEZMBcGA1UEChMQQ2xvdWRmbGFyZSwgSW5jLjEgMB4GA1UEAxMX
-----END CERTIFICATE-----
-----BEGIN CERTIFICATE-----
Q2xvdWRmbGFyZSBJbmMgRUNDIENBLTMwHhcNMjIxMjMxMDAwMDAwWhcNMjMwMzMx
MjM1OTU5WjBqMQswCQYDVQQGEwJVUzETMBEGA1UECBMKQ2FsaWZvcm5pYTEWMBQG
-----END CERTIFICATE-----
-----BEGIN CERTIFICATE-----
A1UEBxMNU2FuIEZyYW5jaXNjbzEZMBcGA1UEChMQQ2xvdWRmbGFyZSwgSW5jLjET
MBEGA1UEAxMKZ2l0bGFiLmNvbTBZMBMGByqGSM49AgEGCCqGSM49AwEHA0IABA4e
-----END CERTIFICATE-----
'''

########################################################################


class TestCommons(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        logging.disable(logging.CRITICAL)

    @classmethod
    def tearDownClass(cls):
        logging.disable(logging.NOTSET)

    # _generate_token

    def test_generate_token(self):
        token, pub = startup._generate_token()
        self.assertIsNotNone(token)
        self.assertIsNotNone(pub)

    # start_services

    def test_start_services_processok(self):
        mock = lambda x, _: [x]
        with patch('opentf.scripts.startup.parse_and_start', mock):
            what = startup.start_services({'services': ['a/b', '${{ CORE }}/c']})
        self.assertEqual(len(what), 2)
        self.assertEqual(what[0], ['a/b'])
        self.assertNotEqual(what[1], '${{ CORE }}/c')
        self.assertTrue(what[1][0].endswith('/c'))

    # start_plugins

    def test_start_plugins_processok_nodisabled(self):
        mock = lambda x, *_: [x]
        with patch('opentf.scripts.startup.parse_and_start', mock):
            what = startup.start_plugins({'plugins': ['a/b', '${{ CORE }}/c']})
        self.assertEqual(len(what), 2)
        self.assertEqual(what[0], ['a/b'])
        self.assertNotEqual(what[1], '${{ CORE }}/c')
        self.assertTrue(what[1][0].endswith('/c'))

    def test_start_plugins_processok_disabled(self):
        mock = lambda x, *_: [x]
        with patch('opentf.scripts.startup.parse_and_start', mock):
            what = startup.start_plugins(
                {'plugins': ['a/b', '${{ CORE }}/c', 'd'], 'disabled': ['D']}
            )
        self.assertEqual(len(what), 3)
        self.assertEqual(what[0], ['a/b'])
        self.assertNotEqual(what[1], '${{ CORE }}/c')
        self.assertTrue(what[1][0].endswith('/c'))

    def test_start_plugins_processok_disabled_aggregated(self):
        mock = MagicMock(return_value=['fOo'])
        with patch('opentf.scripts.startup.parse_and_start', mock):
            what = startup.start_plugins(
                {
                    'plugins': ['a/b', '${{ CORE }}/c', 'd'],
                    'disabled': ['D'],
                    'aggregated': ['d', 'F'],
                }
            )
        self.assertEqual(len(what), 3)
        self.assertEqual(mock.call_count, 3)
        self.assertIn('d', mock.call_args_list[0][0][2])
        self.assertIn('f', mock.call_args_list[0][0][2])

    # start_microservice

    def test_start_microservice_noenv(self):
        mock_result = MagicMock()
        mock_result.pid = 123
        mock_popen = MagicMock(return_value=mock_result)
        with patch('subprocess.Popen', mock_popen):
            what = startup.start_microservice('abc def')
        self.assertEqual(what.pid, 123)
        mock_popen.assert_called_once()
        args = mock_popen.call_args[0][0]
        self.assertEqual(len(args), 4)

    def test_start_microservice_authmode(self):
        mock_result = MagicMock()
        mock_result.pid = 123
        mock_popen = MagicMock(return_value=mock_result)
        with patch('subprocess.Popen', mock_popen), patch(
            'opentf.scripts.startup.OPENTF_AUTHORIZATION_MODE', 'yada'
        ):
            what = startup.start_microservice('abc def')
        self.assertEqual(what.pid, 123)
        mock_popen.assert_called_once()
        args = mock_popen.call_args[0][0]
        self.assertEqual(len(args), 6)

    def test_start_microservice_policyfile(self):
        mock_result = MagicMock()
        mock_result.pid = 123
        mock_popen = MagicMock(return_value=mock_result)
        with patch('subprocess.Popen', mock_popen), patch(
            'opentf.scripts.startup.OPENTF_AUTHORIZATION_POLICY_FILE', 'yada'
        ):
            what = startup.start_microservice('abc def')
        self.assertEqual(what.pid, 123)
        mock_popen.assert_called_once()
        args = mock_popen.call_args[0][0]
        self.assertEqual(len(args), 6)

    def test_start_microservice_tokens(self):
        mock_result = MagicMock()
        mock_result.pid = 123
        mock_popen = MagicMock(return_value=mock_result)
        with patch('subprocess.Popen', mock_popen), patch(
            'opentf.scripts.startup.OPENTF_TOKEN_AUTH_FILE', 'yada'
        ):
            what = startup.start_microservice('abc def')
        self.assertEqual(what.pid, 123)
        mock_popen.assert_called_once()
        args = mock_popen.call_args[0][0]
        self.assertEqual(len(args), 6)

    # maybe_start_eventbus

    def test_maybe_start_eventbus_notneeded(self):
        self.assertFalse(startup.maybe_start_eventbus({}))

    def test_maybe_start_eventbus_ok(self):
        mock_sm = MagicMock(return_value='fOo')
        with patch('opentf.scripts.startup.start_microservice', mock_sm), patch(
            'time.sleep'
        ):
            what = startup.maybe_start_eventbus({'eventbus': {}})
        self.assertEqual(what, ['fOo'])

    # wait_for_eventbus

    def test_wait_for_eventbus_timeout(self):
        mock_sm = MagicMock(return_value='fOo')
        mock_result = MagicMock()
        mock_result.status_code = 400
        mock_get = MagicMock(return_value=mock_result)
        with patch('opentf.scripts.startup.start_microservice', mock_sm), patch(
            'requests.get', mock_get
        ), patch('opentf.scripts.startup.get_eventbus_endpoint'), patch(
            'opentf.scripts.startup.HEALTHCHECK_DELAY', 3
        ), patch(
            'time.sleep'
        ), patch(
            'time.monotonic', MagicMock(side_effect=[0, 1, 2, 3, 4, 5, 6])
        ):
            self.assertRaises(SystemExit, startup.wait_for_eventbus, {'eventbus': {}})

    def test_wait_for_eventbus_external_timeout(self):
        mock_sm = MagicMock(return_value='fOo')
        mock_result = MagicMock()
        mock_result.status_code = 400
        mock_get = MagicMock(return_value=mock_result)
        with patch('opentf.scripts.startup.start_microservice', mock_sm), patch(
            'requests.get', mock_get
        ), patch('opentf.scripts.startup.get_eventbus_endpoint'), patch(
            'opentf.scripts.startup.HEALTHCHECK_DELAY', 3
        ), patch(
            'time.sleep'
        ), patch(
            'time.monotonic', MagicMock(side_effect=[0, 1, 2, 3, 4, 5, 6])
        ):
            self.assertRaises(SystemExit, startup.wait_for_eventbus, {})
        self.assertEqual(mock_get.call_count, 4)
        mock_get.assert_called_with(
            'http://127.0.0.1:38368/subscriptions', headers=None, timeout=10
        )

    def test_wait_for_eventbus_external_modifiedhostport_timeout(self):
        mock_sm = MagicMock(return_value='fOo')
        mock_result = MagicMock()
        mock_result.status_code = 400
        mock_get = MagicMock(return_value=mock_result)
        with patch('opentf.scripts.startup.start_microservice', mock_sm), patch(
            'requests.get', mock_get
        ), patch('opentf.scripts.startup.get_eventbus_endpoint'), patch(
            'opentf.scripts.startup.HEALTHCHECK_DELAY', 3
        ), patch(
            'time.sleep'
        ), patch(
            'time.monotonic', MagicMock(side_effect=[0, 1, 2, 3, 4, 5, 6])
        ), patch(
            'os.environ',
            {'BUS_PORT': '888', 'BUS_HOST': 'example.com', 'BUS_TOKEN': 'secret'},
        ):
            self.assertRaises(SystemExit, startup.wait_for_eventbus, {})
        self.assertEqual(mock_get.call_count, 4)
        mock_get.assert_called_with(
            'http://example.com:888/subscriptions',
            headers={'Authorization': 'Bearer secret'},
            timeout=10,
        )

    # dump_environment

    def test_dump_environment_1(self):
        mock_info = MagicMock()
        with patch('logging.info', mock_info):
            startup.dump_environment()
        mock_info.assert_called()

    def test_dump_environment_3(self):
        mock_info = MagicMock()
        with patch('logging.info', mock_info), patch('os.environ', ENV_SSH_FULL):
            startup.dump_environment()
        self.assertGreater(mock_info.call_count, len(ENV_SSH_FULL))

    # check_environment

    def test_check_environment_none(self):
        startup.check_environment()

    def test_check_environment_nok(self):
        with patch('opentf.scripts.startup.OPENTF_TOKEN_AUTH_FILE', 'xx'):
            self.assertRaises(SystemExit, startup.check_environment)

    def test_check_environment_noknoabac(self):
        with patch('opentf.scripts.startup.OPENTF_TOKEN_AUTH_FILE', 'xx'), patch(
            'opentf.scripts.startup.OPENTF_AUTHORIZATION_MODE', 'JWT'
        ):
            self.assertRaises(SystemExit, startup.check_environment)

    def test_check_environment_okabac(self):
        with patch('opentf.scripts.startup.OPENTF_TOKEN_AUTH_FILE', 'xx'), patch(
            'opentf.scripts.startup.OPENTF_AUTHORIZATION_MODE', 'ABAC'
        ), patch('opentf.scripts.startup.os.path.isfile', MagicMock(return_value=True)):
            startup.check_environment()

    def test_check_environment_nokabacnofile(self):
        with patch('opentf.scripts.startup.OPENTF_TOKEN_AUTH_FILE', 'xx'), patch(
            'opentf.scripts.startup.OPENTF_AUTHORIZATION_MODE', 'ABAC'
        ), patch(
            'opentf.scripts.startup.os.path.isfile', MagicMock(return_value=False)
        ):
            self.assertRaises(SystemExit, startup.check_environment)

    # maybe_generate_token

    def test_maybe_generate_token_bad_public_key(self):
        mock_pubkey = mock_open()
        mock_true = MagicMock(return_value=True)
        with patch('builtins.open', mock_pubkey), patch(
            'os.path.exists', mock_true
        ), patch('os.environ', {'PUBLIC_KEY': 'abCDef'}), patch(
            'os.listdir', mock_true
        ):
            self.assertRaises(SystemExit, startup.maybe_generate_token)

    def test_maybe_generate_token_no_got_good_public_key(self):
        mock_pubkey = mock_open()
        mock_generate = MagicMock(return_value=('FoO', 'bAr'))
        mock_true = MagicMock(return_value=True)
        with patch('builtins.open', mock_pubkey), patch(
            'os.path.exists', mock_true
        ), patch('os.environ', {'PUBLIC_KEY': 'FoO abCDef'}), patch(
            'os.listdir', mock_true
        ), patch(
            'opentf.scripts.startup._generate_token', mock_generate
        ):
            startup.maybe_generate_token()
        mock_pubkey.assert_called_once()
        mock_generate.assnot_called_once()

    def test_maybe_generate_token_yes_got_no_public_key(self):
        mock_pubkey = mock_open()
        mock_generate = MagicMock(return_value=('FoO', 'bAr'))
        mock_true = MagicMock(return_value=True)
        mock_false = MagicMock(return_value=False)
        with patch('builtins.open', mock_pubkey), patch(
            'os.path.exists', mock_true
        ), patch('os.listdir', mock_false), patch(
            'opentf.scripts.startup._generate_token', mock_generate
        ):
            startup.maybe_generate_token()
        mock_pubkey.assert_called_once()
        mock_generate.assert_called_once()

    # maybe populate keystore

    def test_maybe_populate_keystore_bundle_none(self):
        mock_empty = MagicMock(return_value='')
        with patch('os.environ', {'CURL_CA_BUNDLE': None}), patch(
            'os.path.isfile', mock_empty
        ):
            startup.maybe_populate_keystore()
        mock_empty.assert_not_called()

    def test_maybe_populate_keystore_bundle_ko(self):
        mock_empty = MagicMock(return_value='')
        with patch('os.environ', {'CURL_CA_BUNDLE': 'xxYyZ'}), patch(
            'os.path.isfile', mock_empty
        ):
            self.assertRaises(SystemExit, startup.maybe_populate_keystore)
        mock_empty.assert_called_once()

    def test_maybe_populate_keystore_ok(self):
        mock_ca_bundle_rw = mock_open(read_data=CA_BUNDLE)
        mock_true = MagicMock(return_value=True)
        mock_add_cert = MagicMock()
        with patch('os.environ', {'CURL_CA_BUNDLE': 'testOK.crt'}), patch(
            'os.path.isfile', mock_true
        ), patch('builtins.open', mock_ca_bundle_rw), patch(
            'opentf.scripts.startup.add_keystore_certificate', mock_add_cert
        ):
            startup.maybe_populate_keystore()

        mock_ca_bundle_rw.assert_called_once()
        self.assertEqual(
            mock_add_cert.call_count,
            CA_BUNDLE.count('-----END CERTIFICATE-----'),
        )
        call_list = mock_add_cert.call_args_list
        for call_nr in range(len(call_list)):
            self.assertEqual(call_nr, call_list[call_nr].args[0])
            self.assertIn('-----BEGIN CERTIFICATE-----', call_list[call_nr].args[1])
            self.assertIn('-----END CERTIFICATE-----', call_list[call_nr].args[1])

    def test_add_keystore_certificate_ok(self):
        mock_run = MagicMock()
        with patch('subprocess.run', mock_run):
            startup.add_keystore_certificate(1, 'blah')

        mock_run.assert_called_once()
        self.assertEqual('keytool', mock_run.call_args[0][0][0])
        self.assertIn('opentf:1_', mock_run.call_args[0][0][3])

    def test_add_keystore_certificate_fwrite_ko(self):
        mock_file = MagicMock()
        mock_file.name = 'aTemporaryFile'
        mock_file.write = MagicMock(side_effect=IOError())
        mock_context = MagicMock()
        mock_context.__enter__ = MagicMock(return_value=mock_file)
        mock_ntf = MagicMock(return_value=mock_context)

        with patch('opentf.scripts.startup.tempfile.NamedTemporaryFile', mock_ntf):
            self.assertRaises(SystemExit, startup.add_keystore_certificate, 0, 'blah')

        mock_ntf.assert_called_with('w')
        mock_file.write.assert_called_once()

    def test_add_keystore_certificate_ko(self):
        mock_run = MagicMock(
            side_effect=CalledProcessError(
                returncode=1, cmd=['foo'], output=MagicMock()
            )
        )
        with patch('subprocess.run', mock_run):
            self.assertRaises(SystemExit, startup.add_keystore_certificate, 1, 'blah')
            mock_run.assert_called_once()

    # get_eventbus_endpoint

    def test_get_eventbus_endpoint_fromconf(self):
        mock_conf = mock_open(read_data=SSHEE_YAML)
        with patch('builtins.open', mock_conf):
            self.assertEqual(
                startup.get_eventbus_endpoint(), 'http://127.0.0.1:7786/subscriptions'
            )

    def test_get_eventbus_endpoint_default(self):
        mock_conf = mock_open()
        with patch('builtins.open', mock_conf):
            self.assertEqual(
                startup.get_eventbus_endpoint(), 'http://127.0.0.1:38368/subscriptions'
            )

    # write_ini_file

    def test_write_ini_file_1(self):
        mock_ini = mock_open()
        with patch('builtins.open', mock_ini):
            startup.write_init_file()

    # wait_for_observer

    def test_wait_for_observer_timeout(self):
        with patch('opentf.scripts.startup.HEALTHCHECK_DELAY', 1), patch(
            'opentf.scripts.startup.requests', MagicMock()
        ), patch('time.sleep'), patch(
            'time.monotonic', MagicMock(side_effect=[0, 1, 2, 3])
        ):
            self.assertRaises(SystemExit, startup.wait_for_observer)

    def test_wait_for_observer_ok(self):
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.json = lambda: {
            'items': {'foo': {'metadata': {'name': 'observer'}}}
        }
        mock_rg = MagicMock(return_value=mock_response)
        mock_requests = MagicMock()
        mock_requests.get = mock_rg
        with patch('opentf.scripts.startup.HEALTHCHECK_DELAY', 5), patch(
            'opentf.scripts.startup.requests', mock_requests
        ), patch('opentf.scripts.startup.get_eventbus_endpoint', MagicMock()):
            startup.wait_for_observer()

    # show_version

    def test_show_version_nothere(self):
        with patch('logging.warning') as mock_warning, patch(
            'builtins.open', MagicMock(side_effect=IOError('Oops'))
        ):
            startup.show_version()
        mock_warning.assert_called_once_with('Could not read BOM.json, ignoring.')

    def test_show_version_validbutweirdjson(self):
        def _version(package):
            if package == 'opentf-toolkit':
                return 'FOO'
            else:
                raise Exception()

        mock_stat = lambda x: MagicMock(st_mtime=0)
        with patch('logging.info') as mock_info, patch(
            'logging.warning'
        ) as mock_warning, patch(
            'builtins.open', mock_open(read_data='{"foo": "bar"}')
        ), patch(
            'os.stat', mock_stat
        ), patch(
            'opentf.scripts.startup.version', _version
        ):
            startup.show_version()
        mock_warning.assert_not_called()
        self.assertEqual(mock_info.call_count, 3)

    def test_show_version_baseimage(self):
        def _version(package):
            if package == 'opentf-toolkit':
                return 'FOO'
            else:
                raise Exception()

        mock_stat = lambda x: MagicMock(st_mtime=0)
        with patch('logging.info') as mock_info, patch(
            'logging.debug'
        ) as mock_debug, patch('logging.warning') as mock_warning, patch(
            'builtins.open',
            mock_open(
                read_data='{"base-images": [{"name": "bar"}], "external-components": [{"name": "foo"}]}'
            ),
        ), patch(
            'os.stat', mock_stat
        ), patch(
            'opentf.scripts.startup.version', _version
        ):
            startup.show_version()
        mock_warning.assert_not_called()
        self.assertEqual(mock_info.call_count, 4)
        self.assertEqual(mock_debug.call_count, 2)

    # maybe_start_otelcol

    def test_maybe_start_otelcol(self):
        with patch('opentf.scripts.startup._get_env', return_value='yes'), patch(
            'opentf.scripts.startup.logging.info'
        ) as mock_info, patch(
            'opentf.scripts.startup.subprocess'
        ) as mock_sub, patch.dict(
            'os.environ', {'OTELCOL_EXTRA_OPTIONS': '--foo'}
        ):
            startup.maybe_start_otelcol()
        mock_info.assert_called_once_with(
            'Starting OpenTelemetry Collector with following command line options: --foo...'
        )
        mock_sub.Popen.assert_called_once_with(
            'sh -c "/usr/local/bin/otelcol --config=file:/app/otelcol-config.yaml $OTELCOL_EXTRA_OPTIONS"',
            shell=True,
        )

    def test_maybe_start_otelcol_unexpected(self):
        with patch('opentf.scripts.startup._get_env', return_value='bannng'), patch(
            'opentf.scripts.startup.logging.warning'
        ) as mock_warning, patch(
            'opentf.scripts.startup.subprocess'
        ) as mock_sub, patch.dict(
            'os.environ', {'OTELCOL_EXTRA_OPTIONS': '--foo'}
        ):
            startup.maybe_start_otelcol()
        mock_sub.assert_not_called()
        mock_warning.assert_called_once()
        self.assertIn('Unexpected OPENTF_TELEMETRY', mock_warning.call_args[0][0])

    def test_maybe_start_otelcol_exception(self):
        with patch('opentf.scripts.startup._get_env', return_value='yes'), patch(
            'opentf.scripts.startup.subprocess.Popen', side_effect=Exception('Boom')
        ), patch('opentf.scripts.startup.logging.error') as mock_error:
            res = startup.maybe_start_otelcol()
        mock_error.assert_called_once()
        self.assertEqual([], res)

    # watch

    def test_watch_two_dead(self):
        mock_dead = MagicMock()
        mock_dead.poll = MagicMock(return_value=True)
        mock_dead.args = 'oh no'
        mock_dead.returncode = 123
        mock_alive = MagicMock()
        mock_alive.poll = MagicMock(return_value=None)
        running = [mock_alive, mock_alive, mock_dead, mock_alive, mock_dead]
        with patch('time.sleep'), patch('logging.error') as mock_error:
            self.assertRaises(SystemExit, startup.watch, running)
        self.assertEqual(mock_dead.poll.call_count, 3)
        self.assertEqual(mock_error.call_count, 3)

    # main

    def test_main_badversion(self):
        mock_version = MagicMock(return_value='vERsIOn')
        with patch('opentf.scripts.startup.version', mock_version):
            self.assertRaises(SystemExit, startup.main)

    def test_main_noorchestrator(self):
        mock_version = lambda x: {'opentf-toolkit': 1}[x]
        with patch('opentf.scripts.startup.version', mock_version):
            self.assertRaises(SystemExit, startup.main)

    def test_main_keyboardinterruptexits(self):
        mock_conf = mock_open(read_data=OPENTF_YAML)
        mock_version = MagicMock(return_value='vERsIOn')
        with patch('builtins.open', mock_conf), patch(
            'opentf.scripts.startup.maybe_start_eventbus'
        ), patch('opentf.scripts.startup.wait_for_eventbus'), patch(
            'opentf.scripts.startup.maybe_generate_token'
        ), patch(
            'opentf.scripts.startup.dump_environment'
        ), patch(
            'time.sleep', MagicMock(side_effect=KeyboardInterrupt())
        ), patch(
            'opentf.scripts.startup.version', mock_version
        ), patch(
            'opentf.scripts.startup.wait_for_observer'
        ), patch(
            'opentf.scripts.startup.show_version'
        ):
            self.assertRaises(SystemExit, startup.main)
