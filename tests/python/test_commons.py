# Copyright (c) 2022 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Expressions unit tests."""

import logging
import unittest

from unittest.mock import Mock, MagicMock, patch, mock_open

from opentf import commons
from opentf.commons import auth, schemas, pubsub

# pylint: disable=missing-function-docstring

########################################################################

WORKFLOW_1 = {'jobs': {'job_1': {}, 'job_2': {}}}

WORKFLOW_DEP = {'jobs': {'job_1': {}, 'job_2': {'needs': ['job_1']}}}

WORKFLOW_UNKNONWN = {'jobs': {'job_1': {}, 'job_2': {'needs': ['job_3']}}}

WORKFLOW_CIRCULAR = {
    'jobs': {
        'job_1': {'needs': ['job_2']},
        'job_2': {'needs': 'job_1'},
    }
}

APP_NAME = 'mYapP'
CONTEXT_1 = {'eventbus': {}, 'host': 'hOSt', 'port': 'pORt'}
CONTEXT_2 = {
    'eventbus': {'endpoint': 'eNDpoinT', 'token': 'TokEn'},
    'host': 'hOSt',
    'port': 'pORt',
}

BAR_TOKEN = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJpc3MiOiJmb28iLCJzdWIiOiJiQXIifQ.lQ4n4q6HIEnbuLp9wL3crR0QwuUd96eJ5TB4bHQaPncNxAwFR7rkEP3eYnWZtsnGjclKcsdUou7IXTAbSbOpp9KmGaPtPPuYl6x4Z5FQ6Cx55oKjyDpXSxtE-cHq34frINTdetM3tFY3dXWjUY1LU0T04hUxHYke3Cro2E__pTbvQUphuvdLYj_5vCsN5k1IStATv6OvqOJhhxiEkFyNoi1ysnGgeQ8eVD4d9r1yWG-qVpPOuPYRfZI_gonMcvaGgLd2LAVTsSVi02CudAg55W-QlH0Gq0T37PPHyo_wI_ivChuju_6yTCjtIArA65ws4cQfMylHlCUUr9_8FzaFwC0BlPc8S4f72b62dGFi85iO9DFPxz4NkrQo9-fIZ5w_lEAP10KlTLUmErhC6RKIDgzt4PI3EnQEQc6ahylD5yhoNhfbrShJd4tT0r4pnG2s4HqSxgI2t4QxySwAOjjDQo5dJ9bVR4fggRyLf0945PCk7aSVPP8cveCvhgKI19--7K1QH0H_zi5h_VTgkx5s6nsEz3Omh1ardGQd1fXXOrnj0odV3z_WiVGFbnfL0hUjC3CXjMyUUzN6XFf3zf3arPNujiWSxRgagU4DDjsr-V0OZQqGxITXbtCFfOCaaTbxLWmUMpe1vJItdZP0Q4Ju9gMYmW5BgLvUE_KwDIGQ6bk'

BAR_TOKEN_PAYLOAD = {'iss': 'foo', 'sub': 'bAr'}

PUBKEY = '''-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA6w0EICUY+hd+WBmu4qUR
lcqwJlEyr1vjp0RCWlAlEolHCJl6fhoUxhcxgb9GJS6CR0hEGer9tNTW1Ltukjhg
S+9aPnWYC8MPvHEIezYcJNF7Wp9665QMjKRUUcKJrEgQHgX1u44G3IjH4b0dPahl
c0KR2oqLq7VW1LUIOysoOVMbTmJxqsEiaLTb5gvAS2qi9aEyQs1rdzgWyDBYfS9n
52NNOxBPMGHUEsiO/eVX1Bmuq1AzNJYOLoDZL2vjlDvvHvz699x9NfpFG/Gnh6QJ
/QiTZsSKluxIWv7528GOwqUP3gdBvqL5bAergc2rlxAe2G4G9Lyaf+DT8L6KZct/
1aplDULsRq+1RoCY7aqNFoPT3sQETOZFadsBK/VkvJC3QtH2Zc9KmFz6YQPRT39s
kvQApkmRsho17M62crtB2wWvLy4c4YZYIC0Mea95/ATILc+RUElr7A0oLd775oor
rBK0J6mn+af/zIiCtGDcQiCGPG1htnGgI6OmHLCKWqMh/2D4o38+7FQHv2ygEK/Y
tAb1iQBLZz84DPREtZeDKMZiTkP6cDQTl97UPq3R9LP2jCAMgAjWLORAv/aVKruN
e8wgibI3xFrh0BP81d7S/DRgmuo4mmcpb4DF5rdLrWhyySpQGW41OOESEVJZh7el
I7y2crTgPXSw7jlBJaSAg5ECAwEAAQ==
-----END PUBLIC KEY-----'''

POLICY_FILE_OK = '''{"apiVersion": "abac.opentestfactory.org/v1alpha1", "kind": "Policy", "spec": {"user": "alice", "namespace": "*", "resource": "*", "apiGroup": "*"}}
{"apiVersion": "abac.opentestfactory.org/v1alpha1", "kind": "Policy", "spec": {"user": "bob", "namespace": "projectCaribou", "resource": "workflows", "readonly": true}}'''

POLICY_FILE_OK_COMMENTS = '''{"apiVersion": "abac.opentestfactory.org/v1alpha1", "kind": "Policy", "spec": {"user": "alice", "namespace": "*", "resource": "*", "apiGroup": "*"}}
#{"apiVersion": "abac.opentestfactory.org/v1alpha1", "kind": "Policy", "spec": {"user": "bob", "namespace": "projectCaribou", "resource": "workflows", "readonly": true}}'''

POLICY_FILE_NOK_BADSPEC = '''{"apiVersion": "abac.opentestfactory.org/v1alpha1", "kind": "Policy", "spec": {"user": "alice", "namespace": "*", "resource": "*", "apiGroup": "*"}}
{"apiVersion": "abac.opentestfactory.org/v1alpha1", "kind": "Policy", "spec": {"user": "bob", "group": "bad", "namespace": "projectCaribou", "resource": "workflows", "readonly": true}}'''

POLICY_FILE_NOK = '''"apiVersion": "abac.opentestfactory.org/v1alpha1", "kind": "Policy", "spec": {"user": "alice", "namespace": "*", "resource": "*", "apiGroup": "*"}
{"apiVersion": "abac.opentestfactory.org/v1alpha1", "kind": "Policy", "spec": {"user": "bob", "namespace": "projectCaribou", "resource": "workflows", "readonly": true}}'''

TOKEN_FILE_OK = '''token_1,user_name_1,uid_1
token_2,user_name_2,uid_2,"a,b,c"
token_3,user_name_3,uid_3,"c,e"'''

TOKEN_FILE_OK_COMMENTS = '''#token_1,user_name_1,uid_1
token_2,user_name_2,uid_2,"a,b,c"
token_3,user_name_3,uid_3,"c,e"'''

TOKEN_FILE_NOK = '''token_1,user_name_1
token_2,user_name_2,uid_2,"a,b,c"
token_3,user_name_3,uid_3,"c,e"'''

TRUSTED_KEYS_FILE_OK = '''/etc/opentf/trusted_key.pub,Main key,,"namespace-a,namespace-b"
/etc/opentf/another_key.pub,Secondary key,"a,b"
/etc/opentf/yak.pub,Tertiary key'''

TRUSTED_KEYS_FILE_OK_COMMENTS = '''/etc/opentf/trusted_key.pub,Main key,,"namespace-a,namespace-b"
#/etc/opentf/another_key.pub,Secondary key,"a,b"
/etc/opentf/yak.pub,Tertiary key'''

TRUSTED_KEYS_FILE_SPACES_OK = '''/etc/opentf/trusted_key.pub,Main key, ,  "namespace-a, namespace-b"
/etc/opentf/another_key.pub,Secondary key, "a,b"

/etc/opentf/yak.pub, Tertiary key'''

TRUSTED_KEYS_FILE_NOK = '''/etc/opentf/trusted_key.pub,Main key,,"namespace-a,namespace-b"
/etc/opentf/another_key.pub
/etc/opentf/yak.pub,Tertiary key'''


TRUSTED_KEYS_FILE_DUPLICATES = '''/etc/opentf/trusted_key.pub,Main key,,"namespace-a,namespace-b"
/etc/opentf/another_key.pub,Another
/etc/opentf/yak.pub,Tertiary key
/etc/opentf/another_key.pub,Another,,"namespace-c"'''

URC_NS1FOORO_NS2STAR = {
    'user1': [
        {'namespace': 'namespace1', 'resource': 'foo', 'readonly': True},
        {'namespace': 'namespace2', 'resource': '*'},
    ]
}

UNC_NS1_NS2 = {'user1': ['namespace1', 'namespace2']}

########################################################################


def _make_g_with_user1_payload():
    mock_g = MagicMock()
    mock_g.get = lambda _: None
    mock_g.__contains__ = lambda _, x: x == 'payload'
    mock_g.payload = {'sub': 'user1'}
    return mock_g


class TestCommons(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        logging.disable(logging.CRITICAL)

    @classmethod
    def tearDownClass(cls):
        logging.disable(logging.NOTSET)

    # validate_pipeline

    def test_vp_pipeline_ok(self):
        status, what = commons.validate_pipeline(WORKFLOW_1)

        self.assertTrue(status)
        self.assertIsInstance(what, list)
        self.assertEqual(len(what), 1)

    def test_vp_pipeline_nok(self):
        status, _ = commons.validate_pipeline(WORKFLOW_CIRCULAR)

        self.assertFalse(status)

    def test_vp_pipeline_ordered(self):
        status, what = commons.validate_pipeline(WORKFLOW_DEP)

        self.assertTrue(status)
        self.assertIsInstance(what, list)
        self.assertEqual(len(what), 2)
        self.assertEqual(what[0], ['job_1'])
        self.assertEqual(what[1], ['job_2'])

    def test_vp_pipeline_baddeps(self):
        status, _ = commons.validate_pipeline(WORKFLOW_UNKNONWN)

        self.assertFalse(status)

    # make_app

    def test_make_app_ok(self):
        mock_read = mock_open(read_data='yada')
        with patch('builtins.open', mock_read), patch(
            'sys.argv', ['', '--trusted-authorities', 'foo']
        ):
            app = commons.make_app('nAmE', 'dESCriptIOn', '')

        self.assertIn('CONTEXT', app.config)

    def test_make_app_badauthnauthz(self):
        with patch('sys.argv', ['']), patch(
            'opentf.commons.initialize_authn_authz',
            MagicMock(side_effect=commons.ConfigError('ooOops')),
        ):
            self.assertRaises(SystemExit, commons.make_app, 'nAmE', 'dESCriptIOn', '')

    def test_make_app_badconfig(self):
        mock_read = mock_open(read_data='yada')
        with patch('builtins.open', mock_read), patch('sys.argv', ['']), patch(
            'opentf.commons.config.os.path.isfile', MagicMock(return_value=True)
        ), patch(
            'opentf.commons.schemas.validate_schema',
            MagicMock(return_value=(False, None)),
        ):
            self.assertRaises(
                SystemExit, commons.make_app, 'nAmE', 'dESCriptIOn', 'aFile'
            )

    def test_make_app_badcontext(self):
        mock_read = mock_open(
            read_data='{"current-context": "default", "contexts": []}'
        )
        with patch('builtins.open', mock_read), patch('sys.argv', ['']), patch(
            'opentf.commons.os.path.isfile', MagicMock(return_value=True)
        ), patch(
            'opentf.commons.validate_schema', MagicMock(return_value=(True, None))
        ):
            self.assertRaises(SystemExit, commons.make_app, 'nAmE', 'dESCriptIOn', '')

    # make_event

    def test_make_event_simpleversion(self):
        what = commons.make_event('a/b')

        self.assertIn('apiVersion', what)
        self.assertIn('kind', what)
        self.assertEqual(what['apiVersion'], 'a')
        self.assertEqual(what['kind'], 'b')

    def test_make_event_compositeversion(self):
        what = commons.make_event('apiVersion/v1/b')

        self.assertIn('apiVersion', what)
        self.assertIn('kind', what)
        self.assertEqual(what['apiVersion'], 'apiVersion/v1')
        self.assertEqual(what['kind'], 'b')

    def test_make_event_fields(self):
        what = commons.make_event('a/b', foo='fOo', bar=['bAr', 'BAr'])

        self.assertIn('apiVersion', what)
        self.assertIn('kind', what)
        self.assertEqual(what['apiVersion'], 'a')
        self.assertEqual(what['kind'], 'b')
        self.assertIn('foo', what)
        self.assertIn('bar', what)
        self.assertEqual(len(what['bar']), 2)

    # make_status_response

    def test_make_status_response_success(self):
        mock = lambda x, _: x
        with patch('opentf.commons.make_response', mock):
            what = commons.make_status_response('OK', 'meSSage')

        self.assertIn('kind', what)
        self.assertEqual(what['kind'], 'Status')
        self.assertEqual(what['status'], 'Success')

    def test_make_status_response_failure(self):
        mock = MagicMock()
        with patch('opentf.commons.make_response', lambda x, _: x), patch(
            'logging.warning', mock
        ):
            what = commons.make_status_response('Invalid', 'meSSage')

        self.assertIn('kind', what)
        self.assertEqual(what['kind'], 'Status')
        self.assertEqual(what['status'], 'Failure')
        mock.assert_called_once()

    # annotate_response

    def test_annotate_response_nothing(self):
        mock_response = MagicMock()
        mock_response.headers = {}
        _ = commons.annotate_response(mock_response)
        self.assertNotIn('X-Processed-Query', mock_response.headers)

    def test_annotate_response_notused(self):
        mock_response = MagicMock()
        mock_response.headers = {}
        mock_request = MagicMock()
        mock_request.args = {}
        with patch('opentf.commons.request', mock_request):
            _ = commons.annotate_response(mock_response, processed=['foo', 'bar'])
        self.assertNotIn('X-Processed-Query', mock_response.headers)

    def test_annotate_response_used(self):
        mock_response = MagicMock()
        mock_response.headers = {}
        mock_request = MagicMock()
        mock_request.args = {'foO': {}, 'bar': 12}
        with patch('opentf.commons.request', mock_request):
            _ = commons.annotate_response(mock_response, processed=['foO', 'bar'])
        self.assertIn('X-Processed-Query', mock_response.headers)
        self.assertIn('foO', mock_response.headers['X-Processed-Query'])
        self.assertIn('bar', mock_response.headers['X-Processed-Query'])

    def test_annotate_response_someused(self):
        mock_response = MagicMock()
        mock_response.headers = {}
        mock_request = MagicMock()
        mock_request.args = {'bar': 12}
        with patch('opentf.commons.request', mock_request):
            _ = commons.annotate_response(mock_response, processed=['foO', 'bar'])
        self.assertIn('X-Processed-Query', mock_response.headers)
        self.assertNotIn('foO', mock_response.headers['X-Processed-Query'])
        self.assertIn('bar', mock_response.headers['X-Processed-Query'])

    # make_subscription

    def test_make_subscription_emptyselector(self):
        self.assertRaises(
            ValueError,
            pubsub.make_subscription,
            'nAmE',
            {},
            'tArgET',
            context=CONTEXT_1,
        )

    def test_make_subscription_noselector(self):
        what = pubsub.make_subscription('nAmE', None, 'tArgET', context=CONTEXT_1)
        self.assertEqual(what['kind'], 'Subscription')
        self.assertEqual(what['metadata']['name'], 'nAmE')
        self.assertIn('creationTimestamp', what['metadata'])
        self.assertNotIn('selector', what['spec'])

    def test_make_subscription_simple(self):
        what = pubsub.make_subscription(
            'nAmE', {'matchKind': 'kInd'}, 'tArgET', context=CONTEXT_1
        )

        self.assertEqual(what['kind'], 'Subscription')
        self.assertEqual(what['metadata']['name'], 'nAmE')
        self.assertIn('creationTimestamp', what['metadata'])
        self.assertEqual(what['spec']['selector'], {'matchKind': 'kInd'})

    def test_make_subscription_invalid(self):
        self.assertRaises(
            ValueError,
            pubsub.make_subscription,
            'nAmE',
            {'oops': 'oh no'},
            'tArgET',
            context=CONTEXT_1,
        )

    # subscribe

    def test_subscribe_all(self):
        mock_response = MagicMock()
        mock_response.json = lambda: {'details': {'uuid': 'UuID'}}
        mock_response.status_code = 201
        mock_post = MagicMock(return_value=mock_response)
        mock_app = MagicMock()
        mock_app.name = APP_NAME
        mock_app.config = {'CONTEXT': CONTEXT_2}
        with patch('opentf.commons.pubsub.post', mock_post):
            what = commons.subscribe(None, 'tARgET', mock_app)

        self.assertEqual(what, 'UuID')
        mock_post.assert_called_once()
        response = mock_post.call_args[1]['json']
        self.assertNotIn('selector', response['spec'])

    def test_subscribe_kind(self):
        mock_response = MagicMock()
        mock_response.json = lambda: {'details': {'uuid': 'UuID'}}
        mock_response.status_code = 201
        mock_post = MagicMock(return_value=mock_response)
        mock_app = MagicMock()
        mock_app.name = APP_NAME
        mock_app.config = {'CONTEXT': CONTEXT_2}
        with patch('opentf.commons.pubsub.post', mock_post):
            what = commons.subscribe('kINd', 'tARgET', mock_app)

        self.assertEqual(what, 'UuID')
        mock_post.assert_called_once()
        response = mock_post.call_args[1]['json']
        self.assertEqual(response['spec']['selector']['matchKind'], 'kINd')
        self.assertNotIn('matchFields', response['spec']['selector'])

    def test_subscribe_kindversion(self):
        mock_response = MagicMock()
        mock_response.json = lambda: {'details': {'uuid': 'UuID'}}
        mock_response.status_code = 201
        mock_post = MagicMock(return_value=mock_response)
        mock_app = MagicMock()
        mock_app.name = APP_NAME
        mock_app.config = {'CONTEXT': CONTEXT_2}
        with patch('opentf.commons.pubsub.post', mock_post):
            what = commons.subscribe('a/kINd', 'tARgET', mock_app)

        self.assertEqual(what, 'UuID')
        mock_post.assert_called_once()
        response = mock_post.call_args[1]['json']
        self.assertEqual(response['spec']['selector']['matchKind'], 'kINd')
        self.assertEqual(response['spec']['selector']['matchFields']['apiVersion'], 'a')

    def test_subscribe_kindversionlabels(self):
        mock_response = MagicMock()
        mock_response.json = lambda: {'details': {'uuid': 'UuID'}}
        mock_response.status_code = 201
        mock_post = MagicMock(return_value=mock_response)
        mock_app = MagicMock()
        mock_app.name = APP_NAME
        mock_app.config = {'CONTEXT': CONTEXT_2}
        with patch('opentf.commons.pubsub.post', mock_post):
            what = commons.subscribe(
                'a/kINd', 'tARgET', mock_app, labels={'a': 'b'}, fields={'c': 'd'}
            )

        self.assertEqual(what, 'UuID')
        mock_post.assert_called_once()
        response = mock_post.call_args[1]['json']
        self.assertEqual(response['spec']['selector']['matchKind'], 'kINd')
        self.assertIn('matchFields', response['spec']['selector'])
        self.assertIn('matchLabels', response['spec']['selector'])

    def test_subscribe_kindversionexpressions(self):
        mock_response = MagicMock()
        mock_response.json = lambda: {'details': {'uuid': 'UuID'}}
        mock_response.status_code = 201
        mock_post = MagicMock(return_value=mock_response)
        mock_app = MagicMock()
        mock_app.name = APP_NAME
        mock_app.config = {'CONTEXT': CONTEXT_2}
        with patch('opentf.commons.pubsub.post', mock_post):
            what = commons.subscribe(
                'a/kINd',
                'tARgET',
                mock_app,
                expressions=[{'key': 'a', 'operator': 'Exists'}],
                fieldexpressions=[
                    {'key': 'd.c', 'operator': 'NotIn', 'values': ['foo', 'bar']}
                ],
            )

        self.assertEqual(what, 'UuID')
        mock_post.assert_called_once()
        response = mock_post.call_args[1]['json']
        self.assertEqual(response['spec']['selector']['matchKind'], 'kINd')
        self.assertIn('matchFieldExpressions', response['spec']['selector'])
        self.assertIn('matchExpressions', response['spec']['selector'])

    def test_subscribe_badrequest(self):
        mock_response = MagicMock()
        mock_response.status_code = 400
        mock_request = MagicMock(return_value=mock_response)
        mock_app = MagicMock()
        mock_app.name = APP_NAME
        mock_app.config = {'CONTEXT': CONTEXT_2}
        mock_app.logger = MagicMock()
        mock_app.logger.error = MagicMock()
        with patch('opentf.commons.pubsub.post', mock_request):
            self.assertRaises(
                SystemExit,
                commons.subscribe,
                'a/kINd',
                'tARgET',
                mock_app,
                labels={'a': 'b'},
                fields={'c': 'd'},
            )
        mock_app.logger.error.assert_called_once()
        self.assertEqual(
            mock_app.logger.error.call_args[0][0],
            'Could not subscribe to eventbus: was expecting 201, got status %d: %s.',
        )

    def test_subscribe_badnetwork(self):
        mock_request = MagicMock(side_effect=Exception('foo'))
        mock_app = MagicMock()
        mock_app.name = APP_NAME
        mock_app.config = {'CONTEXT': CONTEXT_2}
        mock_app.logger.error = MagicMock()
        with patch('opentf.commons.pubsub.post', mock_request):
            self.assertRaises(
                SystemExit,
                commons.subscribe,
                'a/kINd',
                'tARgET',
                mock_app,
                labels={'a': 'b'},
                fields={'c': 'd'},
            )

        mock_app.logger.error.assert_called_once()

    def test_make_dispatchqueue(self):
        mock_app = MagicMock()
        mock_queue = MagicMock(return_value='foo')
        with patch('opentf.commons.pubsub.threading'), patch(
            'opentf.commons.pubsub.Queue', mock_queue
        ):
            queue = commons.make_dispatchqueue(mock_app)
        mock_app.logger.debug.assert_called_once()
        self.assertEqual(queue, 'foo')

    def test_dispatch_events(self):
        mock_queue = MagicMock()
        mock_fn = MagicMock(side_effect=Exception())
        mock_app = MagicMock()
        mock_app.logger.error = MagicMock(side_effect=SystemExit())
        mock_sleep = MagicMock(side_effect=Exception())
        with patch('opentf.commons.pubsub.sleep', mock_sleep):
            self.assertRaises(
                SystemExit,
                commons.pubsub._dispatch_events,
                mock_queue,
                mock_fn,
                mock_app,
                None,
            )
        mock_sleep.assert_called_once_with(1)
        mock_fn.assert_called_once()

    # publish

    def test_publish_raw(self):
        mock_post = MagicMock()
        with patch('opentf.commons.pubsub.post', mock_post):
            commons.publish(123, CONTEXT_2)

        mock_post.assert_called_once()

    def test_publish_rawjson(self):
        mock_post = MagicMock()
        publication = {'a': 'AAAA'}
        with patch('opentf.commons.pubsub.post', mock_post):
            commons.publish(publication, CONTEXT_2)

        mock_post.assert_called_once()
        self.assertNotIn('metadata', publication)

    def test_publish_jsonmetadata(self):
        mock_post = MagicMock()
        publication = {'a': 'AAAA', 'metadata': {}}
        with patch('opentf.commons.pubsub.post', mock_post):
            commons.publish(publication, CONTEXT_2)

        mock_post.assert_called_once()
        self.assertIn('creationTimestamp', publication['metadata'])

    # _add_securityheaders

    def test_add_securityheaders(self):
        mock_request = MagicMock()
        mock_request.headers = {}
        what = commons._add_securityheaders(mock_request)

        self.assertIn('Content-Type', what.headers)

    # _make_authenticator

    def test_make_authenticator_emptycontext_outofcontext(self):
        what = commons._make_authenticator({})
        self.assertRaises(RuntimeError, what)

    def test_make_authenticator_invalidheader(self):
        mock_msr = MagicMock()
        mock_request = MagicMock()
        mock_request.headers = MagicMock()
        mock_request.headers.get = lambda _: 'beared FoOOO'
        with patch('opentf.commons.request', mock_request), patch(
            'opentf.commons.make_status_response', mock_msr
        ):
            inner = commons._make_authenticator({})
            _ = inner()

        mock_msr.assert_called_once_with(
            'Unauthorized', 'Invalid Authorization header.'
        )

    def test_make_authenticator_nobearer(self):
        mock_msr = MagicMock()
        mock_request = MagicMock()
        mock_request.headers = MagicMock()
        mock_request.headers.get = lambda _: None
        with patch('opentf.commons.request', mock_request), patch(
            'opentf.commons.make_status_response', mock_msr
        ):
            inner = commons._make_authenticator({})
            _ = inner()

        mock_msr.assert_called_once_with('Unauthorized', 'No Bearer token')

    def test_make_authenticator_ok(self):
        mock_msr = MagicMock()
        mock_request = MagicMock()
        mock_request.headers = MagicMock()
        mock_request.headers.get = lambda _: 'bearer FoOOO'
        mock_checktoken = MagicMock(return_value=None)
        with patch('opentf.commons.request', mock_request), patch(
            'opentf.commons.make_status_response', mock_msr
        ), patch('opentf.commons._check_token', mock_checktoken):
            inner = commons._make_authenticator({'trusted_keys': []})
            what = inner()

        self.assertIsNone(what)
        mock_checktoken.assert_called_once()
        self.assertEqual(mock_checktoken.call_args[0][0], 'bearer FoOOO')
        mock_msr.assert_not_called()

    def test_make_authenticator_okinsecure(self):
        mock_msr = MagicMock()
        mock_request = MagicMock()
        mock_request.remote_addr = 'Remote_addR'
        mock_request.headers = MagicMock()
        mock_checktoken = MagicMock(return_value=None)
        mock_g = MagicMock()
        with patch('opentf.commons.request', mock_request), patch(
            'opentf.commons.make_status_response', mock_msr
        ), patch('opentf.commons._check_token', mock_checktoken), patch(
            'opentf.commons.g', mock_g
        ):
            inner = commons._make_authenticator(
                {'enable_insecure_login': True, 'insecure_bind_address': 'Remote_addR'}
            )
            what = inner()

        self.assertIsNone(what)
        self.assertTrue(mock_g.insecure_login)
        mock_checktoken.assert_not_called()
        mock_msr.assert_not_called()

    # _is_authorizer_required

    def test_is_authorizer_required_notanapp(self):
        self.assertFalse(commons._is_authorizer_required())

    def test_is_authorizer_required_norbac(self):
        mock_app = MagicMock()
        mock_app.config = {'CONTEXT': {}}
        with patch('opentf.commons.current_app', mock_app):
            self.assertFalse(commons._is_authorizer_required())

    def test_is_authorizer_required_rbac(self):
        mock_app = MagicMock()
        mock_app.config = {'CONTEXT': {'authorization_mode': ['foo', 'RBAC']}}
        with patch('opentf.commons.current_app', mock_app):
            self.assertTrue(commons._is_authorizer_required())

    def test_is_authorizer_required_rbac_lowercase(self):
        mock_app = MagicMock()
        mock_app.config = {'CONTEXT': {'authorization_mode': ['foo', 'rbac']}}
        with patch('opentf.commons.current_app', mock_app):
            self.assertFalse(commons._is_authorizer_required())

    # authorizer

    def test_authorized_inapp_insecurelogin(self):
        mock_fun = MagicMock(return_value='rEtUrn')
        mock_app = MagicMock()
        mock_app.config = {'CONTEXT': {'authorization_mode': ['foo', 'RBAC']}}
        with patch('opentf.commons.current_app', mock_app):
            fun = commons.authorizer(resource='foO', verb='list')(mock_fun)
            res = fun()

        self.assertNotEqual(fun, mock_fun)
        self.assertEqual(res, 'rEtUrn')

    def test_authorized_inapp_notabac_token(self):
        mock_fun = MagicMock(return_value='rEtUrn')
        mock_app = MagicMock()
        mock_app.config = {'CONTEXT': {'authorization_mode': ['foo', 'RBAC']}}
        with patch('opentf.commons.current_app', mock_app), patch(
            'opentf.commons.g',
            {'payload': BAR_TOKEN_PAYLOAD, 'namespaces': ['default']},
        ):
            fun = commons.authorizer(resource='foO', verb='list')(mock_fun)
            res = fun()

        self.assertNotEqual(fun, mock_fun)
        self.assertEqual(res, 'rEtUrn')

    def test_authorized_abacnopolicies_token(self):
        mock_fun = MagicMock(return_value='rEtUrn')
        mock_app = MagicMock()
        mock_app.config = {
            'CONTEXT': {
                'authorization_mode': ['foo', 'ABAC'],
                'authorization_policies': [],
            }
        }
        mock_msr = MagicMock()
        users_rules = {}
        with patch('opentf.commons.current_app', mock_app), patch(
            'opentf.commons.g', {'payload': BAR_TOKEN_PAYLOAD}
        ), patch('opentf.commons.make_status_response', mock_msr), patch(
            'opentf.commons.auth.USERIDS_RULES_CACHE', users_rules
        ):
            fun = commons.authorizer(resource='foO', verb='list')(mock_fun)
            _ = fun()

        mock_msr.assert_called_once()
        self.assertEqual(
            mock_msr.call_args_list[0][0][1], 'User bAr is not authorized to list foO.'
        )

    def test_authorized_abacreadonly_token(self):
        mock_fun = MagicMock(return_value='rEtUrn')
        mock_app = MagicMock()
        mock_app.config = {
            'CONTEXT': {
                'authorization_mode': ['foo', 'ABAC'],
                'authorization_policies': [
                    {
                        'spec': {
                            "user": "bAr",
                            "namespace": "*",
                            "resource": "*",
                            "apiGroup": "*",
                        }
                    }
                ],
            }
        }
        mock_msr = MagicMock()
        users_rules = {}
        with patch('opentf.commons.current_app', mock_app), patch(
            'opentf.commons.g', {'payload': BAR_TOKEN_PAYLOAD}
        ), patch('opentf.commons.make_status_response', mock_msr), patch(
            'opentf.commons.auth.USERIDS_RULES_CACHE', users_rules
        ):
            fun = commons.authorizer(resource='foO', verb='list')(mock_fun)
            res = fun()

        mock_msr.assert_not_called()
        self.assertEqual(res, 'rEtUrn')
        self.assertTrue(users_rules)

    def test_authorized_inapp_missingpayload(self):
        mock_fun = MagicMock(return_value='rEtUrn')
        mock_app = MagicMock()
        mock_app.config = {'CONTEXT': {'authorization_mode': ['foo', 'RBAC']}}
        mock_msr = MagicMock()
        with patch('opentf.commons.current_app', mock_app), patch(
            'opentf.commons.g', {'foo': 'fOo'}
        ), patch('opentf.commons.make_status_response', mock_msr):
            fun = commons.authorizer(resource='foO', verb='list')(mock_fun)
            _ = fun()

        self.assertNotEqual(fun, mock_fun)
        mock_msr.assert_called_once()

    # _check_token

    def test_check_token_noauthz(self):
        mock_msr = MagicMock()
        with patch('opentf.commons.make_status_response', mock_msr):
            _ = commons._check_token('', {})

        mock_msr.assert_called_once()

    def test_check_token_junkkey(self):
        mock_msr = MagicMock()
        mock_logging = MagicMock()
        mock_logging.error = MagicMock()
        with patch('opentf.commons.make_status_response', mock_msr), patch(
            'opentf.commons.logging', mock_logging
        ):
            _ = commons._check_token(f'Bearer {BAR_TOKEN}', {'trusted_keys': ['jUNk']})
        self.assertEqual(mock_logging.error.call_count, 2)
        args = mock_logging.error.call_args_list
        self.assertIn('Invalid trusted key', args[0][0][0])
        mock_msr.assert_called_once()

    def test_check_token_junktoken(self):
        mock_msr = MagicMock()
        with patch('opentf.commons.make_status_response', mock_msr):
            _ = commons._check_token('Bearer fOo', {'trusted_keys': [(PUBKEY, ['*'])]})

        mock_msr.assert_called_once()

    def test_check_token_ok(self):
        mock_msr = MagicMock()
        g = MagicMock()
        with patch('opentf.commons.make_status_response', mock_msr), patch(
            'opentf.commons.g', g
        ):
            _ = commons._check_token(
                f'Bearer {BAR_TOKEN}', {'trusted_keys': [(PUBKEY, ['*'])]}
            )

        mock_msr.assert_not_called()
        self.assertEqual(g.payload, BAR_TOKEN_PAYLOAD)

    def test_check_token_ok_evenwithjunkkey(self):
        mock_msr = MagicMock()
        g = MagicMock()
        with patch('opentf.commons.make_status_response', mock_msr), patch(
            'opentf.commons.g', g
        ):
            _ = commons._check_token(
                f'Bearer {BAR_TOKEN}', {'trusted_keys': ['junk', (PUBKEY, ['*'])]}
            )

        mock_msr.assert_not_called()
        self.assertEqual(g.payload, BAR_TOKEN_PAYLOAD)

    def test_check_token_authtokens(self):
        mock_msr = MagicMock()
        g = MagicMock()
        token = 'foo-bar-baz'
        with patch('opentf.commons.make_status_response', mock_msr), patch(
            'opentf.commons.g', g
        ):
            _ = commons._check_token(
                f'Bearer {token}',
                {
                    'trusted_keys': [],
                    'authorization_tokens': [
                        ('foo-bar-baz', 'fooBar@Baz', 'fooBarBaz_id')
                    ],
                    'authorization_mode': ['ABAC'],
                },
            )

        mock_msr.assert_not_called()
        self.assertEqual(g.payload, {'sub': 'fooBarBaz_id'})

    def test_check_token_authtokens_noabac(self):
        mock_msr = MagicMock()
        g = MagicMock()
        token = 'foo-bar-baz'
        with patch('opentf.commons.make_status_response', mock_msr), patch(
            'opentf.commons.g', g
        ):
            _ = commons._check_token(
                f'Bearer {token}',
                {
                    'trusted_keys': [],
                    'authorization_tokens': [
                        ('foo-bar-baz', 'fooBar@Baz', 'fooBarBaz_id')
                    ],
                },
            )

        mock_msr.assert_called_once()

    # _in_group

    def test_in_group_notin(self):
        self.assertFalse(auth._in_group('yada_id', 'foo', {}))

    def test_in_group_inbutnogroup(self):
        context = {'authorization_tokens': [('yada_token', 'yada_user', 'yada_id')]}
        self.assertFalse(auth._in_group('yada_id', 'foo', context))

    def test_in_group_in(self):
        context = {
            'authorization_tokens': [('yada_token', 'yada_user', 'yada_id', 'foo,bar')]
        }
        self.assertTrue(auth._in_group('yada_id', 'foo', context))

    # get_actor

    def test_get_actor(self):
        mock_g = MagicMock()
        mock_g.payload = {'sub': 'bAr', 'bar': '*'}
        mock_g.__contains__ = lambda _, b: b == 'payload'
        with patch('opentf.commons.g', mock_g):
            self.assertEqual(commons.get_actor(), 'bAr')

    def test_get_actor_none(self):
        mock_request = MagicMock()
        mock_request.headers.get = lambda _: None
        with patch('opentf.commons.request', mock_request):
            self.assertIsNone(commons.get_actor())

    # _read_key_files

    def test_read_key_files(self):
        mock_read = mock_open(read_data='yada')
        mock_context = {}
        with patch('builtins.open', mock_read):
            auth._read_key_files(['abc', 'def'], mock_context)

        self.assertEqual(len(mock_context), 1)
        self.assertEqual(mock_context['trusted_keys'][0][1], ['default'])
        self.assertEqual(mock_context['trusted_keys'][1][1], ['default'])

    def test_read_key_files_ns(self):
        mock_read = mock_open(read_data='yada')
        mock_context = {
            'authorization_trustedkeys': [
                ('abc', 'abc key'),
                ('def', 'def key', None, 'ns_a,ns_b'),
            ]
        }
        with patch('builtins.open', mock_read):
            auth._read_key_files(['abc', 'def'], mock_context)

        self.assertEqual(len(mock_context), 2)
        self.assertEqual(mock_context['trusted_keys'][0][1], ['default'])
        self.assertEqual(mock_context['trusted_keys'][1][1], ['ns_a', 'ns_b'])

    def test_read_key_files_skippingall(self):
        mock_read = MagicMock(side_effect=Exception('ooOps'))
        mock_logging = MagicMock()
        mock_logging.error = MagicMock()
        mock_context = {
            'authorization_trustedkeys': [
                ('abc', 'abc key'),
                ('def', 'def key', None, 'ns_a,ns_b'),
            ]
        }
        with patch('builtins.open', mock_read), patch(
            'opentf.commons.logging', mock_logging
        ):
            self.assertRaises(
                commons.ConfigError,
                auth._read_key_files,
                ['abc', 'def'],
                mock_context,
            )

    # _read_authorization_policy_file

    def test_read_authorization_policy_file_notfound(self):
        self.assertRaises(
            commons.ConfigError,
            auth._read_authorization_policy_file,
            'dOesNOtExist',
            {},
        )

    def test_read_authorization_policy_file_ok(self):
        context = {}
        mock_exists = MagicMock(return_value=True)
        mock_conf = mock_open(read_data=POLICY_FILE_OK)
        schemas.get_schema(auth.POLICY)
        with patch('os.path.exists', mock_exists), patch('builtins.open', mock_conf):
            auth._read_authorization_policy_file('fooBar', context)

        self.assertIn('authorization_policies', context)
        self.assertEqual(len(context['authorization_policies']), 2)

    def test_read_authorization_policy_file_ok_comments(self):
        context = {}
        mock_exists = MagicMock(return_value=True)
        mock_conf = mock_open(read_data=POLICY_FILE_OK_COMMENTS)
        schemas.get_schema(auth.POLICY)
        with patch('os.path.exists', mock_exists), patch('builtins.open', mock_conf):
            auth._read_authorization_policy_file('fooBar', context)

        self.assertIn('authorization_policies', context)
        self.assertEqual(len(context['authorization_policies']), 1)

    def test_read_authorization_policy_file_badspec(self):
        context = {}
        mock_exists = MagicMock(return_value=True)
        mock_conf = mock_open(read_data=POLICY_FILE_NOK_BADSPEC)
        schemas.get_schema(auth.POLICY)
        with patch('os.path.exists', mock_exists), patch('builtins.open', mock_conf):
            self.assertRaises(
                commons.ConfigError,
                auth._read_authorization_policy_file,
                'fooBar',
                context,
            )

    def test_read_authorization_policy_file_badjson(self):
        context = {}
        mock_exists = MagicMock(return_value=True)
        mock_conf = mock_open(read_data=POLICY_FILE_NOK)
        with patch('os.path.exists', mock_exists), patch('builtins.open', mock_conf):
            self.assertRaises(
                commons.ConfigError,
                auth._read_authorization_policy_file,
                'fooBar',
                context,
            )

    # _read_trustedkeys_auth_file

    def test_read_trustedkeys_auth_file_ok(self):
        context = {}
        mock_exists = MagicMock(return_value=True)
        mock_conf = mock_open(read_data=TRUSTED_KEYS_FILE_OK)
        with patch('os.path.exists', mock_exists), patch('builtins.open', mock_conf):
            auth._read_trustedkeys_auth_file('fooBar', context)

        self.assertIn('authorization_trustedkeys', context)
        self.assertEqual(len(context['authorization_trustedkeys']), 3)

    def test_read_trustedkeys_auth_file_comments_ok(self):
        context = {}
        mock_exists = MagicMock(return_value=True)
        mock_conf = mock_open(read_data=TRUSTED_KEYS_FILE_OK_COMMENTS)
        with patch('os.path.exists', mock_exists), patch('builtins.open', mock_conf):
            auth._read_trustedkeys_auth_file('fooBar', context)

        self.assertIn('authorization_trustedkeys', context)
        self.assertEqual(len(context['authorization_trustedkeys']), 2)

    def test_read_trustedkeys_auth_file_spacesok(self):
        context = {}
        mock_exists = MagicMock(return_value=True)
        mock_conf = mock_open(read_data=TRUSTED_KEYS_FILE_SPACES_OK)
        with patch('os.path.exists', mock_exists), patch('builtins.open', mock_conf):
            auth._read_trustedkeys_auth_file('fooBar', context)

        self.assertIn('authorization_trustedkeys', context)
        self.assertEqual(len(context['authorization_trustedkeys']), 3)
        self.assertEqual(
            context['authorization_trustedkeys'][0][3], 'namespace-a, namespace-b'
        )
        self.assertEqual(context['authorization_trustedkeys'][2][1], 'Tertiary key')

    def test_read_trustedkeys_auth_file_notfound(self):
        self.assertRaisesRegex(
            commons.ConfigError,
            r'Trusted keys authorization file "dOesNOtExist" does not exist.',
            auth._read_trustedkeys_auth_file,
            'dOesNOtExist',
            {},
        )

    def test_read_trustedkeys_auth_file_notenoughelements(self):
        context = {}
        mock_exists = MagicMock(return_value=True)
        mock_conf = mock_open(read_data=TRUSTED_KEYS_FILE_NOK)
        with patch('os.path.exists', mock_exists), patch('builtins.open', mock_conf):
            self.assertRaisesRegex(
                commons.ConfigError,
                r'Entries in trusted keys authorization file "fooBar" must have at least 2 elements:',
                auth._read_trustedkeys_auth_file,
                'fooBar',
                context,
            )

    def test_read_trustedkeys_auth_file_duplicated(self):
        context = {}
        mock_exists = MagicMock(return_value=True)
        mock_conf = mock_open(read_data=TRUSTED_KEYS_FILE_DUPLICATES)
        with patch('os.path.exists', mock_exists), patch('builtins.open', mock_conf):
            self.assertRaisesRegex(
                commons.ConfigError,
                r'Duplicated entries in trusted keys authorization file "fooBar".',
                auth._read_trustedkeys_auth_file,
                'fooBar',
                context,
            )

    def test_read_trustedkeys_auth_file_noentry(self):
        context = {}
        mock_exists = MagicMock(return_value=True)
        mock_conf = mock_open(read_data='\n\n\n\n')
        with patch('os.path.exists', mock_exists), patch('builtins.open', mock_conf):
            self.assertRaisesRegex(
                commons.ConfigError,
                r'No entry found in trusted keys authorization file',
                auth._read_trustedkeys_auth_file,
                'fooBar',
                context,
            )

    # _read_token_auth_file

    def test_read_token_auth_file_notfound(self):
        self.assertRaisesRegex(
            commons.ConfigError,
            r'Token authorization file "dOesNOtExist" does not exist.',
            auth._read_token_auth_file,
            'dOesNOtExist',
            {},
        )

    def test_read_token_auth_file_ok(self):
        context = {}
        mock_exists = MagicMock(return_value=True)
        mock_conf = mock_open(read_data=TOKEN_FILE_OK)
        with patch('os.path.exists', mock_exists), patch('builtins.open', mock_conf):
            auth._read_token_auth_file('fooBar', context)

        self.assertIn('authorization_tokens', context)
        self.assertEqual(len(context['authorization_tokens']), 3)

    def test_read_token_auth_file_ok_comments(self):
        context = {}
        mock_exists = MagicMock(return_value=True)
        mock_conf = mock_open(read_data=TOKEN_FILE_OK_COMMENTS)
        with patch('os.path.exists', mock_exists), patch('builtins.open', mock_conf):
            auth._read_token_auth_file('fooBar', context)

        self.assertIn('authorization_tokens', context)
        self.assertEqual(len(context['authorization_tokens']), 2)

    def test_read_token_auth_file_notenoughelements(self):
        context = {}
        mock_exists = MagicMock(return_value=True)
        mock_conf = mock_open(read_data=TOKEN_FILE_NOK)
        with patch('os.path.exists', mock_exists), patch('builtins.open', mock_conf):
            self.assertRaisesRegex(
                commons.ConfigError,
                r'Entries in token authorization file "fooBar" must have at least 3 elements:',
                auth._read_token_auth_file,
                'fooBar',
                context,
            )

    def test_read_token_auth_file_notentry(self):
        context = {}
        mock_exists = MagicMock(return_value=True)
        mock_conf = mock_open(read_data='\n\n\n\n\n')
        with patch('os.path.exists', mock_exists), patch('builtins.open', mock_conf):
            self.assertRaisesRegex(
                commons.ConfigError,
                r'No entry found in token authorization file "fooBar".',
                auth._read_token_auth_file,
                'fooBar',
                context,
            )

    # initialize_authn_authz

    def test_initialize_authn_authz_insecureonly(self):
        mock_args = MagicMock()
        mock_args.enable_insecure_login = True
        mock_args.trustedkeys_auth_file = None
        mock_args.trusted_authorities = 'foo'
        context = {}
        mock_read = mock_open(read_data='yada')
        with patch('builtins.open', mock_read):
            commons.initialize_authn_authz(mock_args, context)

        self.assertIn('enable_insecure_login', context)
        self.assertTrue(context['enable_insecure_login'])
        self.assertIn('default', context['trusted_keys'][0][1])

    def test_initialize_authn_authz_abacandrbac(self):
        mock_args = MagicMock()
        mock_args.authorization_mode = 'ABAC, RBAC'
        mock_args.trustedkeys_auth_file = None
        context = {}
        self.assertRaisesRegex(
            commons.ConfigError,
            'Cannot specify both ABAC and RBAC as authorization mode',
            commons.initialize_authn_authz,
            mock_args,
            context,
        )

    def test_initialize_authn_authz_abacnopolicy(self):
        mock_args = MagicMock()
        mock_args.authorization_mode = 'ABAC'
        mock_args.authorization_policy_file = False
        mock_args.token_auth_file = 'tokenAuthFile'
        mock_args.trustedkeys_auth_file = None
        context = {}
        self.assertRaises(
            commons.ConfigError, commons.initialize_authn_authz, mock_args, context
        )

    def test_initialize_authn_authz_abacnoauthfile(self):
        mock_args = MagicMock()
        mock_args.authorization_mode = 'ABAC'
        mock_args.authorization_policy_file = 'authorizationPolicyFile'
        mock_args.token_auth_file = False
        mock_args.trustedkeys_auth_file = None
        context = {}
        self.assertRaises(
            commons.ConfigError, commons.initialize_authn_authz, mock_args, context
        )

    def test_initialize_authn_authz_abacok(self):
        mock_args = MagicMock()
        mock_args.authorization_mode = 'ABAC'
        mock_args.authorization_policy_file = 'authorizationPolicyFile'
        mock_args.token_auth_file = 'tokenAuthFile'
        mock_args.trustedkeys_auth_file = None
        mock_args.trusted_authorities = 'foo,bar'
        mock_auth = MagicMock()
        mock_toke = MagicMock()
        mock_trus = MagicMock()
        context = {}
        mock_read = mock_open(read_data='yada')
        with patch('builtins.open', mock_read), patch(
            'opentf.commons.auth._read_token_auth_file', mock_toke
        ), patch(
            'opentf.commons.auth._read_authorization_policy_file', mock_auth
        ), patch(
            'opentf.commons.auth._read_trustedkeys_auth_file', mock_trus
        ):
            commons.initialize_authn_authz(mock_args, context)

        mock_auth.assert_called_once()
        mock_toke.assert_called_once()
        mock_trus.assert_not_called()
        self.assertEqual(len(context['trusted_keys']), 2)
        self.assertIn('default', context['trusted_keys'][0][1])

    # is_user_authorized

    def test_is_authorized_readonly_star(self):
        user = BAR_TOKEN_PAYLOAD['sub']
        cache = {user: [{'resource': '*', 'readonly': True}]}
        with patch('opentf.commons.auth.USERIDS_RULES_CACHE', cache):
            self.assertTrue(commons.is_user_authorized(user, 'res', 'get', {}))

    def test_is_authorized_readonly_nope(self):
        user = BAR_TOKEN_PAYLOAD['sub']
        cache = {
            user: [
                {'resource': 'foo', 'readonly': True},
                {'resource': 'bar', 'readonly': True},
            ]
        }
        with patch('opentf.commons.auth.USERIDS_RULES_CACHE', cache):
            self.assertFalse(commons.is_user_authorized(user, 'res', 'get', {}))

    def test_is_authorized_readonly_second(self):
        user = BAR_TOKEN_PAYLOAD['sub']
        cache = {
            user: [
                {'resource': 'foo', 'readonly': True},
                {'resource': 'res', 'readonly': True},
            ]
        }
        with patch('opentf.commons.auth.USERIDS_RULES_CACHE', cache):
            self.assertTrue(commons.is_user_authorized(user, 'res', 'get', {}))

    def test_is_authorized_readonly_secondstar(self):
        user = BAR_TOKEN_PAYLOAD['sub']
        cache = {
            user: [
                {'resource': 'foo', 'readonly': True},
                {'resource': '*', 'readonly': True},
            ]
        }
        with patch('opentf.commons.auth.USERIDS_RULES_CACHE', cache):
            self.assertTrue(commons.is_user_authorized(user, 'res', 'get', {}))

    def test_is_authorized_readonly_star_needwrite(self):
        user = BAR_TOKEN_PAYLOAD['sub']
        cache = {user: [{'resource': '*', 'readonly': True}]}
        with patch('opentf.commons.auth.USERIDS_RULES_CACHE', cache):
            self.assertFalse(commons.is_user_authorized(user, 'res', 'create', {}))

    def test_is_authorized_readonly_nope_needwrite(self):
        user = BAR_TOKEN_PAYLOAD['sub']
        cache = {
            user: [
                {'resource': 'foo', 'readonly': True},
                {'resource': 'bar', 'readonly': True},
            ]
        }
        with patch('opentf.commons.auth.USERIDS_RULES_CACHE', cache):
            self.assertFalse(commons.is_user_authorized(user, 'res', 'create', {}))

    def test_is_authorized_readonly_second_needwrite(self):
        user = BAR_TOKEN_PAYLOAD['sub']
        cache = {
            user: [
                {'resource': 'foo', 'readonly': True},
                {'resource': 'res', 'readonly': True},
            ]
        }
        with patch('opentf.commons.auth.USERIDS_RULES_CACHE', cache):
            self.assertFalse(commons.is_user_authorized(user, 'res', 'create', {}))

    def test_is_authorized_readonly_secondstar_needwrite(self):
        user = BAR_TOKEN_PAYLOAD['sub']
        cache = {
            user: [
                {'resource': 'foo', 'readonly': True},
                {'resource': '*', 'readonly': True},
            ]
        }
        with patch('opentf.commons.auth.USERIDS_RULES_CACHE', cache):
            self.assertFalse(commons.is_user_authorized(user, 'res', 'create', {}))

    def test_is_authorized_canwrite_star(self):
        user = BAR_TOKEN_PAYLOAD['sub']
        cache = {user: [{'resource': '*'}]}
        with patch('opentf.commons.auth.USERIDS_RULES_CACHE', cache):
            self.assertTrue(commons.is_user_authorized(user, 'res', 'get', {}))

    def test_is_authorized_canwrite_nope(self):
        user = BAR_TOKEN_PAYLOAD['sub']
        cache = {user: [{'resource': 'foo'}, {'resource': 'bar'}]}
        with patch('opentf.commons.auth.USERIDS_RULES_CACHE', cache):
            self.assertFalse(commons.is_user_authorized(user, 'res', 'get', {}))

    def test_is_authorized_canwrite_second(self):
        user = BAR_TOKEN_PAYLOAD['sub']
        cache = {user: [{'resource': 'foo'}, {'resource': 'res'}]}
        with patch('opentf.commons.auth.USERIDS_RULES_CACHE', cache):
            self.assertTrue(commons.is_user_authorized(user, 'res', 'get', {}))

    def test_is_authorized_canwrite_secondstar(self):
        user = BAR_TOKEN_PAYLOAD['sub']
        cache = {user: [{'resource': 'foo'}, {'resource': '*'}]}
        with patch('opentf.commons.auth.USERIDS_RULES_CACHE', cache):
            self.assertTrue(commons.is_user_authorized(user, 'res', 'get', {}))

    def test_is_authorized_canwrite_star_needwrite(self):
        user = BAR_TOKEN_PAYLOAD['sub']
        cache = {user: [{'resource': '*'}]}
        with patch('opentf.commons.auth.USERIDS_RULES_CACHE', cache):
            self.assertTrue(commons.is_user_authorized(user, 'res', 'create', {}))

    def test_is_authorized_canwrite_nope_needwrite(self):
        user = BAR_TOKEN_PAYLOAD['sub']
        cache = {user: [{'resource': 'foo'}, {'resource': 'bar'}]}
        with patch('opentf.commons.auth.USERIDS_RULES_CACHE', cache):
            self.assertFalse(commons.is_user_authorized(user, 'res', 'create', {}))

    def test_is_authorized_canwrite_second_needwrite(self):
        user = BAR_TOKEN_PAYLOAD['sub']
        cache = {user: [{'resource': 'foo'}, {'resource': 'res'}]}
        with patch('opentf.commons.auth.USERIDS_RULES_CACHE', cache):
            self.assertTrue(commons.is_user_authorized(user, 'res', 'create', {}))

    def test_is_authorized_canwrite_secondstar_needwrite(self):
        user = BAR_TOKEN_PAYLOAD['sub']
        cache = {user: [{'resource': 'foo'}, {'resource': '*'}]}
        with patch('opentf.commons.auth.USERIDS_RULES_CACHE', cache):
            self.assertTrue(commons.is_user_authorized(user, 'res', 'create', {}))

    # can_use_namespace

    def test_can_use_namespace_star(self):
        mock_g = MagicMock()
        mock_g.namespaces = ['*']
        mock_g.__contains__ = lambda _, b: b == 'namespaces'
        with patch('opentf.commons.g', mock_g):
            self.assertTrue(commons.can_use_namespace('foo'))

    def test_can_use_namespace_star_in_second_position(self):
        mock_g = MagicMock()
        mock_g.namespaces = ['bar', '*']
        mock_g.__contains__ = lambda _, b: b == 'namespaces'
        with patch('opentf.commons.g', mock_g):
            self.assertTrue(commons.can_use_namespace('foo'))

    def test_can_use_namespace_star_andexact(self):
        mock_g = MagicMock()
        mock_g.namespaces = ['foo', '*']
        mock_g.__contains__ = lambda _, b: b == 'namespaces'
        with patch('opentf.commons.g', mock_g):
            self.assertTrue(commons.can_use_namespace('foo'))

    def test_can_use_namespace_nostar_andexact(self):
        mock_g = MagicMock()
        mock_g.namespaces = ['bar', 'foo']
        mock_g.__contains__ = lambda _, b: b == 'namespaces'
        with patch('opentf.commons.g', mock_g):
            self.assertTrue(commons.can_use_namespace('foo'))

    def test_can_use_namespace_nostar_nomatch(self):
        mock_g = MagicMock()
        mock_g.namespaces = ['bar', 'foo']
        mock_g.__contains__ = lambda _, b: b == 'namespaces'
        mock_g.get = lambda _: False
        with patch('opentf.commons.g', mock_g):
            self.assertFalse(commons.can_use_namespace('baz'))

    def test_can_use_namespace_abac_star(self):
        mock_g = MagicMock()
        mock_g.payload = {'sub': 'subJeCt'}
        mock_g.__contains__ = lambda _, b: b == 'payload'
        with patch('opentf.commons.g', mock_g), patch(
            'opentf.commons.auth.USERIDS_RULES_CACHE', {'subJeCt': [{'namespace': '*'}]}
        ), patch('opentf.commons.auth.USERIDS_NAMESPACES_CACHE', {}):
            self.assertTrue(commons.can_use_namespace('foo'))

    def test_can_use_namespace_abac_star_andexact(self):
        mock_g = MagicMock()
        mock_g.payload = {'sub': 'subJeCt'}
        mock_g.__contains__ = lambda _, b: b == 'payload'
        with patch('opentf.commons.g', mock_g), patch(
            'opentf.commons.auth.USERIDS_RULES_CACHE',
            {'subJeCt': [{'namespace': '*'}, {'namespace': 'foo'}]},
        ), patch('opentf.commons.auth.USERIDS_NAMESPACES_CACHE', {}):
            self.assertTrue(commons.can_use_namespace('foo'))

    def test_can_use_namespace_abac_exact_andstar(self):
        mock_g = MagicMock()
        mock_g.payload = {'sub': 'subJeCt'}
        mock_g.__contains__ = lambda _, b: b == 'payload'
        with patch('opentf.commons.g', mock_g), patch(
            'opentf.commons.auth.USERIDS_RULES_CACHE',
            {'subJeCt': [{'namespace': 'foo'}, {'namespace': '*'}]},
        ), patch('opentf.commons.auth.USERIDS_NAMESPACES_CACHE', {}):
            self.assertTrue(commons.can_use_namespace('foo'))

    def test_can_use_namespace_abac_nostar_nomatch(self):
        mock_g = Mock()
        mock_g.payload = {'sub': 'subJeCt'}
        mock_g.get = lambda _: False
        mock_g.__contains__ = lambda _, b: b in ('payload', 'insecure_login')
        mock_app = MagicMock()
        mock_app.config = {'CONTEXT': {}}
        with patch('opentf.commons.g', mock_g), patch(
            'opentf.commons.auth.USERIDS_RULES_CACHE',
            {'subJeCt': [{'namespace': 'foo'}, {'namespace': 'bar'}]},
        ), patch('opentf.commons.auth.USERIDS_NAMESPACES_CACHE', {}), patch(
            'opentf.commons.current_app', mock_app
        ):
            self.assertFalse(commons.can_use_namespace('baz'))

    def test_can_use_namespace_abac_resource_verb_nope(self):
        mock_g = Mock()
        mock_g.payload = {'sub': 'subJeCt'}
        mock_g.get = lambda _: False
        mock_g.__contains__ = lambda _, b: b in ('payload', 'insecure_login')
        mock_app = MagicMock()
        mock_app.config = {'CONTEXT': {}}
        with patch('opentf.commons.g', mock_g), patch(
            'opentf.commons.auth.USERIDS_RULES_CACHE',
            {
                'subJeCt': [
                    {'namespace': 'foo', 'resource': 'agents', 'readonly': True},
                    {'namespace': 'bar', 'resource': 'agents'},
                ]
            },
        ), patch('opentf.commons.auth.USERIDS_NAMESPACES_CACHE', {}), patch(
            'opentf.commons.current_app', mock_app
        ):
            self.assertFalse(
                commons.can_use_namespace('foo', resource='agents', verb='create')
            )

    def test_can_use_namespace_abac_resource_verb_yep(self):
        mock_g = Mock()
        mock_g.payload = {'sub': 'subJeCt'}
        mock_g.get = lambda _: False
        mock_g.__contains__ = lambda _, b: b in ('payload', 'insecure_login')
        mock_app = MagicMock()
        mock_app.config = {'CONTEXT': {}}
        with patch('opentf.commons.g', mock_g), patch(
            'opentf.commons.auth.USERIDS_RULES_CACHE',
            {
                'subJeCt': [
                    {'namespace': 'foo', 'resource': 'agents', 'readonly': True},
                    {'namespace': 'bar', 'resource': 'agents'},
                ]
            },
        ), patch('opentf.commons.auth.USERIDS_NAMESPACES_CACHE', {}), patch(
            'opentf.commons.current_app', mock_app
        ):
            self.assertTrue(
                commons.can_use_namespace('bar', resource='agents', verb='create')
            )

    def test_can_use_namespace_unknown(self):
        self.assertTrue(commons.can_use_namespace('foo'))

    # run_app

    def test_run_app(self):
        mock_app = MagicMock()
        mock_app.config = {'CONTEXT': CONTEXT_2, 'DESCRIPTOR': []}
        self.assertRaises(ValueError, commons.run_app, mock_app)

    def test_run_app_debug(self):
        mock_app = MagicMock()
        mock_app.config = {'CONTEXT': CONTEXT_2, 'DESCRIPTOR': []}
        with patch('os.environ', {'DEBUG_LEVEL': 'DEBUG'}):
            self.assertRaises(ValueError, commons.run_app, mock_app)

    # list_accessible_namespaces

    def test_accessible_namespaces_insecure_login(self):
        mock_g = {'insecure_login': True}
        with patch('opentf.commons.g', mock_g):
            result = commons.list_accessible_namespaces()
            self.assertEqual(result, ['*'])

    def test_accessible_namespaces_empty_result(self):
        with patch('opentf.commons.g', {'nope': ...}):
            result = commons.list_accessible_namespaces()
            self.assertEqual(result, [])

    def test_accessible_namespaces_authorities_present(self):
        mock_g = MagicMock()
        mock_g.get = lambda _: None
        mock_g.__contains__ = lambda _, x: x == 'namespaces'
        mock_g.namespaces = ['namespace1', 'namespace2']
        with patch('opentf.commons.g', mock_g):
            result = commons.list_accessible_namespaces()
            self.assertEqual(result, ['namespace1', 'namespace2'])

    def test_accessible_namespaces_payload_present(self):
        mock_g = _make_g_with_user1_payload()
        mock_urc = {
            'user1': [
                {'namespace': 'namespace1', 'resource': '*', 'readonly': True},
                {'namespace': 'namespace2', 'resource': '*', 'readonly': True},
            ]
        }
        mock_app = MagicMock()
        mock_app.config = {'CONTEXT': {}}
        with patch(
            'opentf.commons.auth._cache_userid_rules'
        ) as mock_cache_userid_rules, patch('opentf.commons.g', mock_g), patch(
            'opentf.commons.auth._cache_userid_namespaces'
        ) as mock_cache_userid_namespaces, patch(
            'opentf.commons.auth.USERIDS_RULES_CACHE', mock_urc
        ), patch(
            'opentf.commons.auth.USERIDS_NAMESPACES_CACHE', UNC_NS1_NS2
        ), patch(
            'opentf.commons.current_app', mock_app
        ):
            result = commons.list_accessible_namespaces()

        self.assertEqual(result, ['namespace1', 'namespace2'])
        mock_cache_userid_rules.assert_not_called()
        mock_cache_userid_namespaces.assert_not_called()

    def test_accessible_namespaces_verb_payload_present(self):
        mock_g = _make_g_with_user1_payload()
        mock_app = MagicMock()
        mock_app.config = {'CONTEXT': {}}
        with patch(
            'opentf.commons.auth._cache_userid_rules'
        ) as mock_cache_userid_rules, patch('opentf.commons.g', mock_g), patch(
            'opentf.commons.auth._cache_userid_namespaces'
        ) as mock_cache_userid_namespaces, patch(
            'opentf.commons.auth.USERIDS_RULES_CACHE', URC_NS1FOORO_NS2STAR
        ), patch(
            'opentf.commons.auth.USERIDS_NAMESPACES_CACHE', UNC_NS1_NS2
        ), patch(
            'opentf.commons.current_app', mock_app
        ):
            result = commons.list_accessible_namespaces(resource='bar', verb='create')

        self.assertEqual(result, ['namespace2'])
        mock_cache_userid_rules.assert_not_called()
        mock_cache_userid_namespaces.assert_not_called()

    def test_accessible_namespaces_verb_mixed_payload_present(self):
        mock_g = _make_g_with_user1_payload()
        mock_app = MagicMock()
        mock_app.config = {'CONTEXT': {}}
        with patch(
            'opentf.commons.auth._cache_userid_rules'
        ) as mock_cache_userid_rules, patch('opentf.commons.g', mock_g), patch(
            'opentf.commons.auth._cache_userid_namespaces'
        ) as mock_cache_userid_namespaces, patch(
            'opentf.commons.auth.USERIDS_RULES_CACHE', URC_NS1FOORO_NS2STAR
        ), patch(
            'opentf.commons.auth.USERIDS_NAMESPACES_CACHE', UNC_NS1_NS2
        ), patch(
            'opentf.commons.current_app', mock_app
        ):
            result = commons.list_accessible_namespaces(resource='bar', verb='create')

        self.assertEqual(result, ['namespace2'])
        mock_cache_userid_rules.assert_not_called()
        mock_cache_userid_namespaces.assert_not_called()

    def test_accessible_namespaces_verb_double_payload_present(self):
        mock_g = _make_g_with_user1_payload()
        mock_app = MagicMock()
        mock_app.config = {'CONTEXT': {}}
        with patch(
            'opentf.commons.auth._cache_userid_rules'
        ) as mock_cache_userid_rules, patch('opentf.commons.g', mock_g), patch(
            'opentf.commons.auth._cache_userid_namespaces'
        ) as mock_cache_userid_namespaces, patch(
            'opentf.commons.auth.USERIDS_RULES_CACHE', URC_NS1FOORO_NS2STAR
        ), patch(
            'opentf.commons.auth.USERIDS_NAMESPACES_CACHE', UNC_NS1_NS2
        ), patch(
            'opentf.commons.current_app', mock_app
        ):
            result = commons.list_accessible_namespaces(resource='foo', verb='list')

        self.assertSetEqual(set(result), set(['namespace1', 'namespace2']))
        mock_cache_userid_rules.assert_not_called()
        mock_cache_userid_namespaces.assert_not_called()

    def test_accessible_namespaces_verb_double_payload_present_noro(self):
        mock_g = _make_g_with_user1_payload()
        mock_app = MagicMock()
        mock_app.config = {'CONTEXT': {}}
        with patch(
            'opentf.commons.auth._cache_userid_rules'
        ) as mock_cache_userid_rules, patch('opentf.commons.g', mock_g), patch(
            'opentf.commons.auth._cache_userid_namespaces'
        ) as mock_cache_userid_namespaces, patch(
            'opentf.commons.auth.USERIDS_RULES_CACHE', URC_NS1FOORO_NS2STAR
        ), patch(
            'opentf.commons.auth.USERIDS_NAMESPACES_CACHE', UNC_NS1_NS2
        ), patch(
            'opentf.commons.current_app', mock_app
        ):
            result = commons.list_accessible_namespaces(resource='foo', verb='create')

        self.assertListEqual(result, ['namespace2'])
        mock_cache_userid_rules.assert_not_called()
        mock_cache_userid_namespaces.assert_not_called()

    # _get_contextparameter_spec

    def test_get_contextparameter_spec_ok(self):
        param = 'my_param'
        mock_app = MagicMock()
        mock_app.name = 'appname'
        mock_app.config = {
            'CONTEXT': {'my_other_param': 123},
            'DESCRIPTOR': [
                {
                    'metadata': {'name': 'appname'},
                    'spec': {
                        'contextParameters': [
                            {
                                'name': param,
                                'default': 321,
                                'descriptiveName': 'my param',
                            }
                        ]
                    },
                }
            ],
        }
        what = commons._get_contextparameter_spec(mock_app, param)
        self.assertIsNotNone(what)
        self.assertEqual(what['default'], 321)
        self.assertIn('__PARAMETERS__', mock_app.config)
        self.assertEqual(len(mock_app.config['__PARAMETERS__']), 3)
        names = [x['name'] for x in mock_app.config['__PARAMETERS__']]
        self.assertIn(param, names)

    def test_get_contextparameter_spec_redefinedefault(self):
        param = 'availability_check_delay_seconds'
        mock_app = MagicMock()
        mock_app.name = 'appname'
        mock_app.config = {
            'CONTEXT': {'my_other_param': 123},
            'DESCRIPTOR': [
                {
                    'metadata': {'name': 'appname'},
                    'spec': {
                        'contextParameters': [
                            {
                                'name': param,
                                'default': 321,
                                'descriptiveName': 'my param',
                            }
                        ]
                    },
                }
            ],
        }
        what = commons._get_contextparameter_spec(mock_app, param)
        self.assertIsNotNone(what)
        self.assertEqual(what['default'], 321)
        self.assertIn('__PARAMETERS__', mock_app.config)
        self.assertEqual(len(mock_app.config['__PARAMETERS__']), 2)
        names = [x['name'] for x in mock_app.config['__PARAMETERS__']]
        self.assertIn(param, names)

    def test_get_contextparameter_spec_ignoreotherapp_ok(self):
        param = 'my_param'
        mock_app = MagicMock()
        mock_app.name = 'appname'
        mock_app.config = {
            'CONTEXT': {'my_other_param': 123},
            'DESCRIPTOR': [
                {
                    'metadata': {'name': 'appname'},
                    'spec': {
                        'contextParameters': [
                            {
                                'name': param,
                                'default': 321,
                                'descriptiveName': 'my param',
                            }
                        ]
                    },
                },
                {
                    'metadata': {'name': 'anotherappname'},
                    'spec': {
                        'contextParameters': [
                            {
                                'name': param,
                                'default': 999,
                                'descriptiveName': 'my param',
                            }
                        ]
                    },
                },
            ],
        }
        what = commons._get_contextparameter_spec(mock_app, param)
        self.assertIsNotNone(what)
        self.assertEqual(what['default'], 321)
        self.assertIn('__PARAMETERS__', mock_app.config)
        self.assertEqual(len(mock_app.config['__PARAMETERS__']), 3)

    # get_context_parameter

    def test_get_context_parameter_found(self):
        mock_app = MagicMock()
        mock_app.config = {'CONTEXT': {'my_param': 123}, 'DESCRIPTOR': []}
        what = commons.get_context_parameter(mock_app, 'my_param')
        self.assertEqual(what, 123)

    def test_get_context_parameter_notfoundcalldefault(self):
        mock_app = MagicMock()
        mock_app.config = {'CONTEXT': {'my_param': 123}, 'DESCRIPTOR': []}
        what = commons.get_context_parameter(mock_app, 'my_param2', default=444)
        self.assertEqual(what, 444)

    def test_get_context_parameter_foundinenv(self):
        mock_app = MagicMock()
        mock_app.name = 'my_app'
        mock_app.config = {'CONTEXT': {'my_param': 123}, 'DESCRIPTOR': []}
        with patch('os.environ', {'MY_APP_MY_PARAM': 321}):
            what = commons.get_context_parameter(mock_app, 'my_param')
        self.assertEqual(what, 321)

    def test_get_context_parameter_found_notinenvnotshared(self):
        mock_app = MagicMock()
        mock_app.name = 'my_app'
        mock_app.config = {'CONTEXT': {'my_param2': 123}, 'DESCRIPTOR': []}
        with patch('os.environ', {'MY_PARAM2': 321}):
            what = commons.get_context_parameter(mock_app, 'my_param2')
        self.assertEqual(what, 123)

    def test_get_context_parameter_found_inenvshared(self):
        mock_app = MagicMock()
        mock_app.name = 'appname'
        mock_app.config = {
            'CONTEXT': {'my_param': 123},
            'DESCRIPTOR': [
                {
                    'metadata': {'name': 'appname'},
                    'spec': {
                        'contextParameters': [
                            {
                                'name': 'my_param',
                                'default': 321,
                                'descriptiveName': 'my param',
                                'shared': True,
                            }
                        ]
                    },
                }
            ],
        }
        with patch('os.environ', {'MY_PARAM': 678}):
            what = commons.get_context_parameter(mock_app, 'my_param')
        self.assertEqual(what, 678)

    def test_get_context_parameter_found_inenvshared_app(self):
        mock_app = MagicMock()
        mock_app.name = 'appname'
        mock_app.config = {
            'CONTEXT': {'my_param': 123},
            'DESCRIPTOR': [
                {
                    'metadata': {'name': 'appname'},
                    'spec': {
                        'contextParameters': [
                            {
                                'name': 'my_param',
                                'default': 321,
                                'descriptiveName': 'my param',
                                'shared': True,
                            }
                        ]
                    },
                }
            ],
        }
        with patch('os.environ', {'MY_PARAM': 678, 'APPNAME_MY_PARAM': 444}):
            what = commons.get_context_parameter(mock_app, 'my_param')
        self.assertEqual(what, 444)

    def test_get_context_parameter_found_deprecated(self):
        mock_app = MagicMock()
        mock_app.name = 'appname'
        mock_app.config = {
            'CONTEXT': {},
            'DESCRIPTOR': [
                {
                    'metadata': {'name': 'appname'},
                    'spec': {
                        'contextParameters': [
                            {
                                'name': 'my_param',
                                'deprecatedNames': ['my_old_param'],
                                'default': 321,
                                'descriptiveName': 'my param',
                                'shared': True,
                            }
                        ]
                    },
                }
            ],
        }
        with patch('os.environ', {'MY_OLD_PARAM': 678}):
            what = commons.get_context_parameter(mock_app, 'my_param')
        self.assertEqual(what, 678)

    def test_get_context_parameter_notfound_deprecated(self):
        mock_app = MagicMock()
        mock_app.name = 'appname'
        mock_app.config = {
            'CONTEXT': {},
            'DESCRIPTOR': [
                {
                    'metadata': {'name': 'appname'},
                    'spec': {
                        'contextParameters': [
                            {
                                'name': 'my_param',
                                'deprecatedNames': ['my_old_param'],
                                'default': 321,
                                'descriptiveName': 'my param',
                                'shared': True,
                            }
                        ]
                    },
                }
            ],
        }
        with patch('os.environ', {'NOT_MY_OLD_PARAM': 678}):
            what = commons.get_context_parameter(mock_app, 'my_param')
        self.assertEqual(what, 321)

    def test_get_context_parameter_notfounddefault(self):
        mock_app = MagicMock()
        mock_app.name = 'appname'
        mock_app.config = {
            'CONTEXT': {'my_other_param': 123},
            'DESCRIPTOR': [
                {
                    'metadata': {'name': 'appname'},
                    'spec': {
                        'contextParameters': [
                            {
                                'name': 'my_param',
                                'default': 321,
                                'descriptiveName': 'my param',
                            }
                        ]
                    },
                }
            ],
        }
        what = commons.get_context_parameter(mock_app, 'my_param')
        self.assertEqual(what, 321)

    def test_get_context_parameter_notfoundnodefault(self):
        mock_app = MagicMock()
        mock_app.name = 'appname'
        mock_app.config = {
            'CONTEXT': {'my_other_param': 123},
            'DESCRIPTOR': [
                {
                    'metadata': {'name': 'appname'},
                    'spec': {
                        'contextParameters': [
                            {
                                'name': 'my_param',
                                'descriptiveName': 'my param',
                            }
                        ]
                    },
                }
            ],
        }
        mock_app.logger = MagicMock()
        mock_app.logger.error = MagicMock()
        self.assertRaises(
            SystemExit, commons.get_context_parameter, mock_app, 'my_param'
        )
        mock_app.logger.error.assert_called_once_with(
            'Context parameter "my_param" not in current context and no default value specified.'
        )

    def test_get_context_parameter_notaninteger(self):
        mock_app = MagicMock()
        mock_app.name = 'appname'
        mock_app.config = {
            'CONTEXT': {'my_param': '123AZE'},
            'DESCRIPTOR': [
                {
                    'metadata': {'name': 'appname'},
                    'spec': {
                        'contextParameters': [
                            {
                                'name': 'my_param',
                                'type': 'int',
                                'descriptiveName': 'my param',
                            }
                        ]
                    },
                }
            ],
        }
        mock_app.logger = MagicMock()
        mock_app.logger.error = MagicMock()
        self.assertRaises(
            SystemExit, commons.get_context_parameter, mock_app, 'my_param'
        )
        mock_app.logger.error.assert_called_once_with(
            'Context parameter "my_param" not an integer: invalid literal for int() with base 10: \'123AZE\'.'
        )

    def test_get_context_parameter_toobig(self):
        mock_app = MagicMock()
        mock_app.name = 'appname'
        mock_app.config = {
            'CONTEXT': {'my_param': 123},
            'DESCRIPTOR': [
                {
                    'metadata': {'name': 'appname'},
                    'spec': {
                        'contextParameters': [
                            {
                                'name': 'my_param',
                                'type': 'int',
                                'descriptiveName': 'my param',
                                'maxValue': 10,
                            }
                        ]
                    },
                }
            ],
        }
        mock_app.logger = MagicMock()
        mock_app.logger.error = MagicMock()
        self.assertRaises(
            SystemExit, commons.get_context_parameter, mock_app, 'my_param'
        )
        mock_app.logger.error.assert_called_once_with(
            'My param must be less that 11.',
        )

    def test_get_context_parameter_toosmall(self):
        mock_app = MagicMock()
        mock_app.name = 'appname'
        mock_app.config = {
            'CONTEXT': {'my_param': 1},
            'DESCRIPTOR': [
                {
                    'metadata': {'name': 'appname'},
                    'spec': {
                        'contextParameters': [
                            {
                                'name': 'my_param',
                                'type': 'int',
                                'descriptiveName': 'my param',
                                'minValue': 10,
                            }
                        ]
                    },
                }
            ],
        }
        mock_app.logger = MagicMock()
        mock_app.logger.error = MagicMock()
        self.assertRaises(
            SystemExit, commons.get_context_parameter, mock_app, 'my_param'
        )
        mock_app.logger.error.assert_called_once_with(
            'My param must be greater than 9.',
        )

    # get_context_service

    def test_get_context_service_found(self):
        mock_app = MagicMock()
        mock_app.config = {'CONTEXT': {'services': {'my_service': 'SOMETHING'}}}
        what = commons.get_context_service(mock_app, 'my_service')
        self.assertEqual(what, 'SOMETHING')

    def test_get_context_service_notfound(self):
        mock_app = MagicMock()
        mock_app.config = {'CONTEXT': {'my_param': '123AZE'}}
        self.assertRaises(
            SystemExit, commons.get_context_service, mock_app, 'my_service'
        )

    # is_uuid

    def test_is_uuid_valid(self):
        self.assertTrue(commons.is_uuid('12345678-1234-5678-1234-567812345678'))

    def test_is_uuid_notvalid(self):
        self.assertFalse(commons.is_uuid('yada12345678-1234-5678-1234-567812345678'))

    # make_uuid

    def test_make_uuid(self):
        uuid = commons.make_uuid()
        self.assertEqual(len(uuid), 36)
        self.assertTrue(commons.is_uuid(uuid))


if __name__ == '__main__':
    unittest.main()
